package encryption;

import static org.junit.Assert.*;
import java.nio.charset.Charset;
import org.junit.Test;

public class EncryptorTest {

	@Test
	public void test_xor_encryption_encrypt_then_decrypt() {
		byte [] code_book = "2b*X.2Q/90;J".getBytes(Charset.forName("UTF-8"));
		String message = "IÕm sorry, Dave. IÕm afraid I canÕt do that.";
		String encrypted_message = Encryptor.cryptXOR(message, code_book);
		String decrypted_message = Encryptor.cryptXOR(encrypted_message, code_book);

		assertFalse(message.equals(encrypted_message));
        	assertEquals("The decrypted message does not equal the original message.", message, decrypted_message);
	}

	@Test
	public void test_xor_encryption_with_different_codebooks() {
		byte [] code_book_1 = "2b*X.2Q/90;J".getBytes(Charset.forName("UTF-8"));
		byte [] code_book_2 = "d4^8W?c0wq;wq]/q3".getBytes(Charset.forName("UTF-8"));

		String message = "IÕm sorry, Dave. IÕm afraid I canÕt do that.";

		String encrypted_message_1 = Encryptor.cryptXOR(message, code_book_1);
		String decrypted_message_1 = Encryptor.cryptXOR(encrypted_message_1, code_book_1);

		String encrypted_message_2 = Encryptor.cryptXOR(message, code_book_2);
		String decrypted_message_2 = Encryptor.cryptXOR(encrypted_message_2, code_book_2);

		assertFalse(encrypted_message_1.equals(encrypted_message_2));
	        assertEquals("The decrypted messages do not equal each other.", decrypted_message_1, decrypted_message_2);
	}


}
