package encryption;

/* Author: Matt Shaffer
 * Class: CIS 4443
 * Program: XOR
 * 
 * This class contains a crypt method which takes a message
 * and encrypts or decrypts a XOR message.
 */

import java.nio.charset.Charset;

public class Encryptor {

	public static String cryptXOR(String message, byte [] codebook) {
		// Takes the message, and does an XOR on the message and returns
		// a string of the XOR'ed message.
		byte [] message_to_bytes = message.getBytes();
		message_to_bytes = crypt_message(message_to_bytes, codebook);
		String encrypted_message = new String(message_to_bytes);

		return encrypted_message;
	}

	private static byte [] crypt_message(byte [] message, byte [] codebook) {
		// Loop through the byte array and crypts each byte individually.
		// Returns the byte array after being XOR'ed.
		Codebook book = new Codebook(codebook);
		for (int i = 0; i < message.length; i++) {
			message[i] = xor_byte(message[i], book.getCodebookValue(i));
		}
		return message;
	}

	private static byte xor_byte(byte b, byte translator_byte) {
		// Does an XOR on the individual bits of a byte,
		// and returns that byte XOR'ed.
		return (byte)(((int) b) ^ ((int) translator_byte));
	}

}
