package encryption;

import java.nio.charset.Charset;

public class Run {

	public static void main(String[] args) {
		byte [] code_book = "3dQ?a/[1afQ".getBytes(Charset.forName("UTF-8"));

		String message = Encryptor.cryptXOR("I�m sorry, Dave. I�m afraid I can�t do that.", code_book);
		System.out.println(String.format("The message encrypted is: %s", message));

		message = Encryptor.cryptXOR(message, code_book);
		System.out.println(String.format("The message decrypted is: %s", message));
	}

}
