package encryption;

/* Author: Matt Shaffer
 * Class: CIS 4443
 * Program: XOR
 * 
 * This class represents code book objects, which just wraps
 * a byte [] and makes it easy to get the correct value from
 * the code book by not making the user have manually look up
 * a value.
 */

public class Codebook {
	private byte [] codebook;
	private int book_length;

	public Codebook(byte [] codebook) {
		this.codebook = codebook;
		book_length = codebook.length;
	}

	public byte getCodebookValue(int n) {
		// Just shifting back to beginning of the code book
		// if the index is past the end of our code book.
		if (n > book_length - 1) {
			n = n % book_length;
		}
		return codebook[n];
	}

}
